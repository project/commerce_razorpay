<?php

namespace Drupal\commerce_razorpay\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Razorpay\Api\Api;
use Razorpay\Api\Errors\SignatureVerificationError;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class Controller.
 *
 * Razorpay controller for payment actions.
 *
 * @package Drupal\commerce_razorpay\Controller
 */
class Controller extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The Request Stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructing variables for payment.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The Request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * Create Method to get services.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container variable to get services.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * Capture Payment function for payments.
   */
  public function capturePayment() {
    $amount = $this->requestStack->getCurrentRequest()->query->get('amount');
    $commerce_order_id = $this->requestStack->getCurrentRequest()->query->get('order_id');
    $payment_settings = json_decode($this->requestStack->getCurrentRequest()->query->get('payment_settings'));
    $response = json_decode($this->requestStack->getCurrentRequest()->query->get('response'));
    $razorpay_signature = $response->razorpay_signature;
    $razorpay_payment_id = $response->razorpay_payment_id;
    $razorpay_order_id = $response->razorpay_order_id;
    $key_id = $payment_settings->key_id;
    $key_secret = $payment_settings->key_secret;

    $api = new Api($key_id, $key_secret);
    $payment = $api->payment->fetch($razorpay_payment_id);
    if ($payment->status == 'authorized') {
      $payment->capture(['amount' => $amount]);
    }
    // @todo Save payment method details in order object.
    // Validating  Signature.
    $success = TRUE;
    $error = "Payment Failed";

    if (empty($razorpay_payment_id) === FALSE) {
      $api = new Api($key_id, $key_secret);
      try {
        $attributes = [
          'razorpay_order_id' => $razorpay_order_id,
          'razorpay_payment_id' => $razorpay_payment_id,
          'razorpay_signature' => $razorpay_signature,
        ];
        $api->utility->verifyPaymentSignature($attributes);
      }
      catch (SignatureVerificationError $e) {
        $success = FALSE;
        $error = 'Razorpay Error : ' . $e->getMessage();
      }
    }

    // If Payment is successfully captured at razorpay end.
    if ($success === TRUE) {
      $message = "Payment ID: {$razorpay_payment_id}";
      $this->messenger()->addStatus($this->t(':message', [':message' => $message]));
    }
    else {
      $message = "Your payment failed " . $error;
      $this->messenger()->addError($this->t(':message', [':message' => $message]));
    }
    $url = Url::fromRoute('commerce_payment.checkout.return', [
      'commerce_order' => $commerce_order_id,
      'step' => 'payment',
    ], ['absolute' => TRUE])->toString();

    return new RedirectResponse($url);
  }

}
